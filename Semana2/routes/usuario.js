var express = require('express');
var router = express.Router();
var ControllerUsuario = require('../controllers/api/ControllerUsuario')

router.get("/",ControllerUsuario.usuario_list);
router.post("/create",ControllerUsuario.usuario_create);
router.post("/reservar",ControllerUsuario.usuario_reservar);


module.exports = router;
