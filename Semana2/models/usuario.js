var mongoose = require('mongoose');
const bcypt = require("bcrypt");
const saltMounds = 10;

const Reserva = require('./reserva');


var Schema = mongoose.Schema;

const validateEmail =  function(email){
    const re = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g;
    return re.test(email)
}
// Declare the Schema of the Mongo model
var UsuarioSchema = new Schema({
    nombre: {
        type: String,
        trim: true,
        required: [true,"El nombre es oblogatorio"],
        unique: true,
        index: true,
    },

    email: {
        type: String,
        required: [true,"EL email es obligatorio"],
        unique: true,
        lowercase: true,
        validate:[validateEmail,"El email no es valido"],
        match:[/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g]
    },
    contraseña: {
        type: String,
        required: true,
    },
    
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado:{
        type:Boolean,
        default:false
    }

});

UsuarioSchema.pre("save",function(next){
    if(this.isModified('contraseña')){
        this.contraseña =bcrypt.hasSync(this.contraseña,saltMounds);
    }
    next();
});

UsuarioSchema.methods.validcontraseña=function(contraseña){
    return bcypt.compareSync(contraseña,this.contraseña);
}

UsuarioSchema.methods.reservar = function (biciId, desde, hasta, cb) {
    var reserva  = new Reserva({
        usuario: this._id,
        bicicleta: biciId,
        desde: desde,
        hasta: hasta
    });
    console.log(reserva);
    reserva.save(cb);
}

//Export the model
module.exports = mongoose.model('Usuario', UsuarioSchema);