var moment = require("moment");
var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var ReservaSchema = new Schema({
        desde: Date,
        hasta: Date,
        bicicleta: {type:Schema.Types.ObjectId, ref:'Bicicleta'},
        usuario : {type:Schema.Types.ObjectId, ref:'Usuario'}
});


ReservaSchema.methods.diasReserva= function () {
        return moment(this.hasta).diff(moment(this.desde),"days")+1;    
}


// Compile model from schema

module.exports = mongoose.model('Reserva', ReservaSchema );