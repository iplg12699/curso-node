var Bicicleta = function (id,color,modelo,ubicacion) {
    this.id=id;
    this.color=color;
    this.modelo=modelo;
    this.ubicacion=ubicacion;
    
}

Bicicleta.prototype.toString = function () {
    return "id:"+ this.id+ "|color: "+this.color;
}

Bicicleta.allBicis=[];
Bicicleta.add= function (aBici) {
    Bicicleta.allBicis.push(aBici);
}


Bicicleta.findById= function (aBiciId) {
    var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
    if (aBici)
        return aBici;
    else
        throw new Error(`No existe la bicicleta con el id ${aBiciId}`);
}

Bicicleta.removeById = function(aBiciId){
    //Bicicleta.findById(aBiciId);
    for (var i = 0;i < Bicicleta.allBicis.length; i++) {
        if (Bicicleta.allBicis[i].id == aBiciId) {
            Bicicleta.allBicis.splice(i,1);
            break;
        }
        
    }
}


var a = new Bicicleta(1,"rojo","urbana",[-3.9975808,-79.2034328]);
var b = new Bicicleta(2,"blanca","urbana",[-3.9966645,-79.2018119]);

Bicicleta.add(a);
Bicicleta.add(b);




module.exports=Bicicleta;
