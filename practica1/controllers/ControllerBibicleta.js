var Bicicleta = require("../models/Bibicleta");

exports.bicicleta_list = function (req, res) {
    res.render('bicicletas/index', {
        bicis: Bicicleta.allBicis
    });
}

exports.bibicleta_create_get = function (req, res) {
    res.render("bicicletas/create");
}

exports.bibicleta_create_post = function (req, res) {
    var body = req.body;
    var bici = new Bicicleta(
        body.id,
        body.color,
        body.modelo,
        [body.lat, body.lng]
    );

    Bicicleta.add(bici);
    res.redirect("/bicicletas");
}

exports.bibicleta_update_get = function (req, res) {
    var bici = Bicicleta.findById(req.params.id);
    res.render("bicicletas/update",{bici});
}

exports.bibicleta_update_post = function (req, res) {
    var body = req.body;
    var bici = Bicicleta.findById(req.params.id);
    bici.id=body.id;
    bici.color = body.color;
    bici.modelo=body.modelo;
    bici.ubicacion= [body.lat, body.lng];
            
    res.redirect("/bicicletas");
}




exports.bicicleta_delete_post= function (req,res) {
    Bicicleta.removeById(req.body.id);
    res.redirect("/bicicletas")
}