var Bicicleta = require("../../models/Bibicleta");

exports.bicicleta_list= function (req,res) {
    res.status(200).json({
        bicicletas:Bicicleta.allBicis
    });
}

exports.bicicleta_create= function (req,res) {
    console.log(req);
    var bici = new Bicicleta(
        req.body.id,
        req.body.color,
        req.body.modelo,
        [req.body.lat,req.body.lng]
    );
    
    console.log(bici);
    Bicicleta.add(bici);
    
    res.status(200).json({
        bicicleta: bici,
        mensaje:"Agregado correctamente"
    })

    
}

exports.bicicleta_update= function (req,res) {
    console.log(req)
    if (req.body.id){
        var bici = Bicicleta.findById(req.body.id);
        bici.color=req.body.color;
        bici.modelo=req.body.modelo;
        bici.ubicacion=[req.body.lat,req.body.lng]
        
        res.status(200).json({
            bicicleta: bici,
            mensaje:"Modificada correctamente"
        })
    }    
    
    res.status(404).json({
        mensaje:"propiedad id no encontrada"
    })
    
}
    

    


exports.bicicleta_delete= function (req,res) {
    Bicicleta.removeById(req.body.id)
    res.status(204).send();
    
}
