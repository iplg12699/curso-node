var mymap = L.map('mapid').setView([-3.9966797,-79.2017614,15], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>'
}).addTo(mymap);


$.ajax({
    dataType:"json",
    url:"/api/bicicletas",
    success:function (result) {
        console.log(result);
        result.bicicletas.forEach(bici => {
            L.marker(bici.ubicacion,{title:bici.id}).addTo(mymap);
        });
    }
})