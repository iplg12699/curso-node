var express = require('express');
var router = express.Router();
var ControllerBicicleta = require("../controllers/ControllerBibicleta");

router.get('/',ControllerBicicleta.bicicleta_list);
router.get('/create',ControllerBicicleta.bibicleta_create_get);
router.post('/create',ControllerBicicleta.bibicleta_create_post);
router.post('/:id/delete',ControllerBicicleta.bicicleta_delete_post);
router.get('/:id/update',ControllerBicicleta.bibicleta_update_get);
router.post('/:id/update',ControllerBicicleta.bibicleta_update_post);


module.exports =router;