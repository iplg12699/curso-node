## Curos De NodeJs Proyecto final

    Uso de autenticacion y login

## Login-test
 
```
    usuario: "test@test.com"
    contraseña:"test"
```


### Rutas de usuario API

* __Obtener lista de usuarios__ 
```json
GET: http://localhost:3000/api/usuarios

header:"content-type":"applicationjson"

result: 
    {"usuarios":[lista de usuarios]}
```

<hr>

* __Crear usuario__ 
```json
Post: http://localhost:3000/api/usuarios/create

header:"content-type":"applicationjson"

body_example :

    {
        "nombre":"Israel",
        "email":"israelpat42@gmail.com",
        "contraseña":"contra123"
    }

result: 
    {
    "usuario": {
        "_id": "5fadc518ae07422ed41281cd",
        "nombre": "Israel",
        "email": "israelpat42@gmail.com",
        "contraseña": "contra123",
        "__v": 0
    },
    "mensaje": "Usuario creado correctamente"
}
```

<hr>

* __Reservar bicicleta-usuarios__ 


```json
Post: http://localhost:3000/api/usuarios/reservar

header:"content-type":"applicationjson"

body_example :

    {
    "id":<id_usuario>,
    "bici_id":<id_bicicleta>,
    "desde":"2020-11-12",
    "hasta":"2020-11-13"
    }

result: 
    {
        "reserva": {
            "_id":  "5fadce5b7b84a33768840aa2",
            "usuario":  "5fadc518ae07422ed41281cd",
            "bicicleta":    "5fadcb13a4f05d2fe8328036",
            "desde":    "2020-11-12T00:00:00.000Z",
            "hasta":    "2020-11-13T00:00:00.000Z",
            "__v": 0
        },
        "mensaje": "Creado  correctamente"
    }
```

### Rutas de bicicleta API

* __Obtener Bicicletas__

```json
GET: http://localhost:3000/api/bicicletas

header:"content-type":"applicationjson"

result: 
    {"bicis":[lista de bicicletas]}
```

<hr>

* __Crear Bicicleta__ 
```json
Post: http://localhost:3000/api/bicicletas/create

header:"content-type":"applicationjson"

body_example :

    {       
        "code": "6",
        "color": "rojo",
        "modelo": "urbana",
        "ltd":"-38.12",
        "lng":"-34.11"
    }

result: 
    {
        "bici": {
            "ubicacion": [
                -38.12,
                -34.11
            ],
            "_id":  "5fadc9bfa4f05d2fe8328035",
            "code": 6,
            "color": "rojo",
            "modelo": "urbana",
            "__v": 0
        },
        "mensaje": "agregada    correctamente"
    }
```

* __Borrar Bicicleta por codigo__ 
```json
DELETE: http://localhost:3000/api/bicicletas/delete

header:"content-type":"applicationjson"

body_example :

   {       
    "code": "6"
    }

result: StatusCode(204)  
    
```

* __Actualizar Bicicleta por codigo__ 
```json
PUT: http://localhost:3000/api/bicicletas/update


header:"content-type":"applicationjson"

body_example :

   {       
    "code": "6",
    "color": "blanco",
    "modelo": "montaña",
    "ltd":"-31.12",
    "lng":"-34.11"
}

result: 
    {
    "bicicleta": {
        "ubicacion": [
            -38.12,
            -34.11
        ],
        "_id": "5fadcb13a4f05d2fe8328036",
        "code": 6,
        "color": "blanco",
        "modelo": "montaña",
        "__v": 0
    },
    "mensaje": "Modificada correctamente"
    }
    
    
resultado si no encuentra code:

    {
    "mensaje": "No se a podido modificar revise los datos"
    }
```



## Test

```
    npm test
```

## RUN

```
    npm run devstart
```
 


## Herramientas 👷

* mongodb version 5.10
* jasmine 
* mongoose
* MomentJs
* Nodemon    
