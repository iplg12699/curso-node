"use strict";
const sgTransporter = require("nodemailer-sendgrid-transport");
const nodemailer = require("nodemailer");

// async..await is not allowed in global scope, must use a wrapper
exports.sendEmail = async function (options_mail) {
  // Generate test SMTP service account from ethereal.email
  // Only needed if you don't have a real mail account for testing
  let mailConfig;
  // create reusable transporter object using the default SMTP transport
  if (
    process.env.NODE_ENV == "production" ||
    process.env.NODE_ENV == "stagging"
  ) {
    var options = {
      auth: {
        api_key: process.env.SENDGRID_API_KEY,
      },
    };

    mailConfig = sgTransporter(options);
    
  } else {
    var options = {
      host: "smtp.ethereal.email",
      port: 587,
      secure: false, // true for 465, false for other ports
      auth: {
        user: "kenton.ritchie89@ethereal.email", // generated ethereal user
        pass: "cEaXDFt6TrGzxrQbH8", // generated ethereal password
      },
    };
    mailConfig = options;
  }

  let transporter = nodemailer.createTransport(mailConfig);
  // send mail with defined transport object
  let info = await transporter.sendMail(
    /*{
        from: '"Fred Foo 👻" <foo@example.com>', // sender address
        to: "bar@example.com, baz@example.com", // list of receivers
        subject: "Hello ✔", // Subject line
        text: "Hello world?", // plain text body
        html: "<b>Hello world?</b>", // html body
      }*/
    options_mail
  );

  console.log("Message sent: %s", info.messageId);
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

  // Preview only available when sending through an Ethereal account
  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
};