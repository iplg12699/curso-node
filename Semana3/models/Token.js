const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Declare the Schema of the Mongo model
var TokenSchema = Schema({
    _usuarioId:{
        type:mongoose.Schema.Types.ObjectId,
        required:true,
        ref:"Usuario"
    },
    token:{
        type:String,
        required:true
    },
    createdAt:{
        type:Date,
        required:true,
        default:Date.now,
        expires:43200

    },
});

//Export the model
module.exports = mongoose.model('Token', TokenSchema);