var express = require('express');
var router = express.Router();
var ControllerBicicleta = require('../../controllers/api/ControllerBicicletaAPI')

router.get("/",ControllerBicicleta.bicicleta_list);
router.post("/create",ControllerBicicleta.bicicleta_create);
router.delete("/delete",ControllerBicicleta.bicicleta_delete);
router.put("/update",ControllerBicicleta.bicicleta_update);


module.exports=router;