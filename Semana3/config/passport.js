require("dotenv").config()
const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const Usuario = require("../models/usuario");
var GoogleStrategy = require( 'passport-google-oauth20' ).Strategy;
const FacebookTokenStrategy = require("passport-facebook-token");

passport.use(new FacebookTokenStrategy({
    clientID: process.env.FACEBOOK_APP_ID,
    clientSecret:process.env.FACEBOOK_APP_SECRET
},function(accessToken,refreshToken,profile,done){
    try{
        Usuario.findOneOrCreateByFacebook(profile,function(err,user){
            if(err) console.log("err"+err);
            return done(err,user);
        });
    }catch(err2){
        console.log(err2);
        return done(err2,null);
    }
}

));

passport.use(new GoogleStrategy({
    clientID:     process.env.GOOGLE_CLIENT_ID,
    clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    callbackURL:  process.env.BASE_URI+"/auth/google/callback",
    passReqToCallback   : true
  },
  function(request, accessToken, refreshToken, profile, done) {
    console.log(profile);
    Usuario.findOrCreateByGoogle(profile, function (err, user) {
      return done(err, user);
    });
  }
));

passport.use(new LocalStrategy({
        usernameField: 'email',
        passwordField: 'contrasena'
    },
    function (username, password, done) {

        Usuario.findOne({
            email: username
        }, function (err, usuario) {
            if (err) {
                return done(err);
            }
            if (!usuario) {                     //email no existe
                return done(null, false, {
                    email:{message: 'Credencial incorrecta'},
                    contrasena:{message: 'Credencial incorrecta'},
                });
            }
            if (!usuario.verifyPassword(password)) {    // pasword erroneo
                return done(null, false, {
                    email:{message: 'Credencial incorrecta'},
                    contrasena:{message: 'Credencial incorrecta'},
                });
            }
            return done(null, usuario);

        });

    }
));

passport.serializeUser(function (usuario, done) {
    done(null, usuario._id);
});

passport.deserializeUser(function (id, done) {
    Usuario.findById(id, function (err, usuario) {
        done(err, usuario);
    })
})

module.exports = passport;